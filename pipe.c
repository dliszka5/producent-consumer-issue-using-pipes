#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>
#include <sys/types.h>

#include <sys/wait.h>

#include <sys/errno.h>



/*
  SYSTEM CALL: pipe();                                                          

  PROTOTYPE: int pipe( int fd[2] );                                             
    RETURNS: 0 on success                                                       
             -1 on error: errno = EMFILE (no free descriptors)                  
                                  EMFILE (system file table is full)            
                                  EFAULT (fd array is not valid)                

  NOTES: fd[0] is set up for reading, fd[1] is set up for writing
*/

#define READ  0
#define WRITE 1

int main(int argc, char* argv[]){
	pid_t customer;
	pid_t producer;

  int numberOfCustomers = 0;
  int numberOfProducers = 0;

  int fd[2];

  int status = 0;

  /*       DEFAULT VALUES       */
  if(argc != 3){
    printf("Setting default values for number of producers and customers\n");
    numberOfProducers = 1;
    numberOfCustomers = 1;
  } else {
    numberOfProducers = atoi(argv[1]) > 0 ? atoi(argv[1]) : -atoi(argv[1]);
    numberOfCustomers = atoi(argv[2]) > 0 ? atoi(argv[2]) : -atoi(argv[2]);
  }

  if(pipe(fd) == -1){
    if(errno == EMFILE){
      perror("No free descriptors\n");
    } else {
      perror("pipe error");
      exit(EXIT_FAILURE);
    }
  }

  if(numberOfCustomers + numberOfCustomers > 200){
    printf("Too many processes!\n");
    printf("Using default numberOfCustomers & numberOfProducers instead\n");

    numberOfProducers = 100;
    numberOfCustomers = 100;

  }

  /*
      fd[0] == 3   for read only
      fd[1] == 4   for write only
  */
  

  printf("* ..:: FORKING PRODUCER ::.. *\n");

  for(int i=0; i < numberOfProducers; ++i){
    switch(producer = fork()){
      case -1   : perror("fork error");
                  exit(EXIT_FAILURE);
                    break;
      case  0   : close(fd[READ]);
                  if(execl("./producer","producer",&fd[WRITE],NULL) == -1){
                      perror("execl error");
                      exit(EXIT_FAILURE);
                    }

                    break;
      default   : printf("Producer created : %d\n",producer);
                    break;
    }
  }

  close(fd[WRITE]);

  printf("* ..:: FORKING CUSTOMER ::.. *\n");

  for(int i=0; i < numberOfCustomers; ++i){
    switch(customer = fork()){
      case -1   : perror("fork error");
                  exit(EXIT_FAILURE);
                  break;

      case  0   : close(fd[WRITE]);
                  if(execl("./customer","customer",&fd[READ],NULL) == -1){
                      perror("execl error");
                      exit(EXIT_FAILURE);
                    }
                  break;
      default   : printf("Customer created : %d\n",customer);
                  break;
    }
  }

  close(fd[READ]);

  pid_t pid;

  for(int i = 0; i < numberOfCustomers + numberOfProducers; ++i){
    pid = wait(&status);
    if(pid == -1){
      perror("wait error");
      exit(EXIT_FAILURE);
    }
    printf("%d terminated\n",pid);
  }
  printf("..:: All proces terminated! ::..\n");

	return 0;
}