#include <stdio.h>
#include <stdlib.h>

#include <unistd.h>

#include <time.h>

/*
	 ssize_t write(int fd, const void *buf, size_t count);
*/

#define data_size 10

int main(int argc, char* argv[]){
	if(argc != 2){
		printf("Wrong number of params given!\n");
		exit(EXIT_FAILURE);
	}

	srand(time(NULL));

	char buf;
	int respond = 0;

	int fd = *argv[1];

	for(int i=0; i<data_size; ++i){
		//buf = (char) (rand()%96 + 32);
		buf = (char) ('a' + rand()%26);
		respond = write(fd,(char*)&buf,sizeof(buf));
		if(respond == -1){
			perror("write error");
			exit(EXIT_FAILURE);
		}
		printf("[P - %d] write : %c\n",getpid(),buf);
	}

	close(fd);

	return 0;
}