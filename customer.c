#include <stdio.h>
#include <stdlib.h>

 #include <unistd.h>

/*
	 ssize_t write(int fd, const void *buf, size_t count);
*/

#define true 1

int main(int argc, char* argv[]){
	if(argc != 2){
		printf("Wrong number of params given!\n");
		exit(EXIT_FAILURE);
	}

	char buf;
	int respond = 0;

	int fd =*argv[1];

	while(true){
		respond = read(fd,(char*)&buf,sizeof(buf));
		if(respond == -1){
			perror("read error");
			exit(EXIT_FAILURE);
		} else if (respond == 0){
			break;
		}
		printf("[C - %d] read : %c\n",getpid(),buf);
	}

	
	close(fd);

	return 0;
}